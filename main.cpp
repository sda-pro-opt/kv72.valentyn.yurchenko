#include <iostream>
#include <map>
#include <string>
#include <stdio.h>
#include <vector>
#include <fstream>
#include <filesystem>


using namespace std;
struct Token {
	string name;
	int code;
	int x;
	int y;
};

vector <Token> Tok_vec;
map <int, int> onesymb = { {9, 0}, {10, 0}, {32, 0}, {40, 3}, {41,5}, {42, 4}, {47, 4}, {59,4}, {58,4}, {92,4} };
map < string, int> multisymb = { {"MOD", 301} };
map < string, int> keywords = { {"PROGRAM", 401}, {"BEGIN", 402}, {"ENG", 403},
{"LOOP", 404}, {"CASE", 405}, {"OF", 406}, {"ENDCASE", 407}};
map <int, int> constants;
map <string, int> identificators;
void table_fill() {
	for (int i = 48; i < 58; i++) onesymb.insert(pair <int, int>(i, 1));
	for (int i = 65; i < 91; i++) onesymb.insert(pair <int, int>(i, 2));
	for (int i = 33; i < 40; i++) onesymb.insert(pair <int, int>(i, 6));
	for (int i = 43; i < 47; i++) onesymb.insert(pair <int, int>(i, 6));
	for (int i = 60; i < 64; i++) onesymb.insert(pair <int, int>(i, 6));
	onesymb.insert(pair <int, int>(91, 6));
	for (int i = 93; i < 256; i++) onesymb.insert(pair <int, int>(i, 6));
}
/*int ConstTabSearch(string buff) {
	map <int, int> ::iterator it;

}*/

int pos_x = 0, pos_y = 1;

/*void take_character() {
	ch = file.get();
	pos_x++;

	if (s == '\n') {
		pos_x = 0;
		pos_y++;
	}
	return ch;
}*/

void Err(int reason, int x, int y, string fOut) {
	ofstream f_out;
	f_out.open(fOut);
	switch (reason) {
	case 0://
		f_out << "Illegal symbol: line " << y <<" colum "<< x << endl;
		break;
	case 1:
		f_out << "Comment isn`t closed: line " << y << " colum " << x << endl;
		break;
	case 2:
		f_out << "In this place must be 'space': line " << y << " colum " << x << endl;
		break;
	}
}
int scaner(string file_name, string fOut) {
	table_fill();
	char s;
	int st_x, st_y = 1;
	int pos_x = 1;
	int pos_y = 1;
	int lexcode;
	string buff;
	ifstream file;
	file.open(file_name);
	if (!file.is_open())
	{
		cout << "File not found\n\n";
		return -1;
	}
	/*for (file >> s; !file.eof(); file >> s)
		cout << s << endl;*/
	map <int, int> ::iterator it;
	map <string, int> ::iterator it_s;
	s = file.get();
	int flag = 0;
	while (!file.eof()) {
		it = onesymb.find(int(s));
		int category{ -1 };
		if (it != onesymb.end())
		{
			category = it->second;
		}
		buff = "";
		if (int(s) == 10) {
			st_y++;
			pos_x = 0;
		}//else pos_x++;
		switch (category) {
		case 0://whitespace
			while (!file.eof()) {
				pos_x++;
				s = file.get();
				it = onesymb.find(int(s));
				if (it->second != 0) break;
			}
			break;
		case 1://const
			st_x = pos_x;
			while (!file.eof() && (it->second) == 1) {
				pos_x++;
				buff = buff + s;
				s = file.get();
				it = onesymb.find(int(s));
			}
			if (it->second != 0) {
				Err(2, pos_x, st_y, fOut);
				return 0;
			}
			it = constants.find(stoi(buff));
			if (it != constants.end()) {
				lexcode = it->second;
			}
			else {
				lexcode = constants.size() + 501;
				constants.insert(pair <int, int>(stoi(buff), lexcode));
			}
			Tok_vec.push_back({ buff, lexcode, st_x, st_y });
			break;
		case 2://ident
			st_x = pos_x;
			while (!file.eof() && ((it->second) == 2 || (it->second) == 1)) {
				buff = buff + s;
				pos_x++;
				s = file.get();
				it = onesymb.find(int(s));
			}
			if (buff == "MOD") {
				lexcode = 301;
				Tok_vec.push_back({ buff, lexcode, st_x, st_y });
				break;
			}
			it_s = keywords.find(buff);
			if (it_s != keywords.end()) lexcode = it_s->second;
			else {
				it_s = identificators.find(buff);
				if (it_s != identificators.end()) lexcode = it_s->second;
				else {
					lexcode = identificators.size() + 1001;
					identificators.insert(pair <string, int>(buff, lexcode));
				}
			}Tok_vec.push_back({ buff, lexcode, st_x, st_y });;
			break;
		case 3://comment
			st_x = pos_x;
			pos_y = st_y;
			s = file.get();
			pos_x++;
			flag = 0;
			if (int(s) != 42) {//!=*
				Err(0, st_x, st_y, fOut);
				return 0;
			}
			else {
				s = file.get();
				if (int(s) == 10) {
					pos_y++;
					pos_x = 0;
				}
				pos_x++;
				while (!file.eof() && !( int(s) == 41 && flag == 1)) {//s == ')' && prev *
					if (int(s) == 42) flag = 1;
					else flag = 0;
					s = file.get();
					if (int(s) == 10) {
						pos_y++;
						pos_x = 0;
					}
					pos_x++;
					//it = onesymb.find(int(s));
				}
				if (file.eof()) {
					Err(1, st_x, st_y, fOut);
					return 0;
				}
			}
			st_y = pos_y;
			s = file.get();
			break;
		case 4://onesymb del
			buff = buff + s;
			st_x = pos_x;
			pos_x++;
			lexcode = int(s);
			s = file.get();
			Tok_vec.push_back({ buff, lexcode, st_x, st_y });
			break;
		default: 
			Err(0, pos_x, st_y, fOut);
			return 0;
			s = file.get();
			break;
		}
		//cout << buff << "       " << lexcode << endl;
		//s = file.get();
	}
	return 1;
}
void vec_print(string fOut) {
	//cout << "lexcode        line  column" << endl;
	//for (int i = 0; i < Tok_vec.size(); i++) {
	//	cout /*<< Tok_vec[i].name << "\t\t"*/ << Tok_vec[i].code << "\t\t" << Tok_vec[i].y << "\t" << Tok_vec[i].x << "\t" << endl;
	//}
	ofstream f_out;
	f_out.open(fOut);
	f_out << "lexcode        line  column" << endl;
	for (int i = 0; i < Tok_vec.size(); i++) {
		f_out /*<< Tok_vec[i].name << "\t\t"*/ << Tok_vec[i].code << "\t\t" << Tok_vec[i].y << "\t" << Tok_vec[i].x << "\t" << endl;
	}
}
namespace fs = std::filesystem;
void tests() {
	const fs::path workdir = fs::current_path();
	const fs::path configPath = workdir / "tests";
	const auto fileName = "input.sig";
	const auto fOutName = "generated.txt";
	for (fs::directory_iterator it(configPath), end; it != end; ++it)
	{
		const auto output = it->path().filename();
		const auto result = it->path().generic_string() + "/" + fileName;
		const auto out = it->path().generic_string() + "/" + fOutName;
		if (scaner(result, out) == 1) vec_print(out);

	}
}
int main() {
	tests();
	//if (scaner("file.txt") == 1) vec_print();
	getchar();
	return 0;
}